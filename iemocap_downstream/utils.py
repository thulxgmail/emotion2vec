import torch

def train_one_epoch(model, optimizer, criterion, train_loader, device):
    model.train()
    train_loss = 0
    plossep = 0
    alossep = 0
    for batch in train_loader:
        ids, net_input, labels = batch["id"], batch["net_input"], batch["labels"]
        feats = net_input["feats"]
        speech_padding_mask = net_input["padding_mask"]

        feats = feats.to(device)

        speech_padding_mask = speech_padding_mask.to(device)
        labels = labels.to(device)
        
        optimizer.zero_grad()
        outputs = model(feats, speech_padding_mask)
        
        loss, ploss, aloss = criterion(outputs, labels)

        train_loss += loss.item()
        plossep += ploss.item()
        alossep += aloss.item()
        
        loss.backward()
        optimizer.step()

    return train_loss, plossep, alossep


def valid_one_epoch(model, criterion, train_loader, device):
    model.eval()
    train_loss = 0
    plossep = 0
    alossep = 0
    for batch in train_loader:
        ids, net_input, labels = batch["id"], batch["net_input"], batch["labels"]
        feats = net_input["feats"]
        speech_padding_mask = net_input["padding_mask"]
        feats = feats.to(device)
        speech_padding_mask = speech_padding_mask.to(device)
        labels = labels.to(device)
        outputs = model(feats, speech_padding_mask)
        loss, ploss, aloss = criterion(outputs, labels)
        train_loss += loss.item()
        plossep += ploss.item()
        alossep += aloss.item()
    return train_loss, plossep, alossep

@torch.no_grad()
def validate_and_test(model, data_loader, device, num_classes):
    model.eval()
    correct, total = 0, 0

    # unweighted accuracy
    unweightet_correct = [0] * num_classes
    unweightet_total = [0] * num_classes

    # weighted f1
    tp = [0] * num_classes
    fp = [0] * num_classes
    fn = [0] * num_classes

    for batch in data_loader:
        ids, net_input, labels = batch["id"], batch["net_input"], batch["labels"]
        feats = net_input["feats"]
        speech_padding_mask = net_input["padding_mask"]

        feats = feats.to(device)

        speech_padding_mask = speech_padding_mask.to(device)
        labels = labels.to(device)

        outputs = model(feats, speech_padding_mask)

        _, predicted = torch.max(outputs.data, 1)

        total += labels.size(0)
        correct += (predicted == labels.long()).sum().item()
        for i in range(len(labels)):
            unweightet_total[labels[i]] += 1
            if predicted[i] == labels[i]:
                unweightet_correct[labels[i]] += 1
                tp[labels[i]] += 1
            else:
                fp[predicted[i]] += 1
                fn[labels[i]] += 1

    weighted_acc = correct / total * 100
    unweighted_acc = compute_unweighted_accuracy(unweightet_correct, unweightet_total) * 100
    weighted_f1 = compute_weighted_f1(tp, fp, fn, unweightet_total) * 100

    return weighted_acc, unweighted_acc, weighted_f1

def inference(model, ):
    pass


def compute_unweighted_accuracy(list1, list2):
    result = []
    for i in range(len(list1)):
        result.append(list1[i] / list2[i])
    return sum(result)/len(result)

def compute_weighted_f1(tp, fp, fn, unweightet_total):
    f1_scores = []
    num_classes = len(tp)
    
    for i in range(num_classes):
        if tp[i] + fp[i] == 0:
            precision = 0
        else:
            precision = tp[i] / (tp[i] + fp[i])
        if tp[i] + fn[i] == 0:
            recall = 0
        else:
            recall = tp[i] / (tp[i] + fn[i])
        if precision + recall == 0:
            f1_scores.append(0)
        else:
            f1_scores.append(2 * precision * recall / (precision + recall))
            
    wf1 = sum([f1_scores[i] * unweightet_total[i] for i in range(num_classes)]) / sum(unweightet_total)
    return wf1


def ccc(y_true, y_pred):
    y_true = torch.squeeze(y_true, dim=-1)
    y_pred = torch.squeeze(y_pred, dim=-1)
    eps = 1e-7
    y_true_mean = torch.mean(y_true)
    y_pred_mean = torch.mean(y_pred)
    cov = torch.sum((y_true - y_true_mean) * (y_pred - y_pred_mean)) / (y_true.shape[0] - 1 + eps)
    ccc = 2 * cov / (torch.var(y_true) + torch.var(y_pred) + (y_true_mean - y_pred_mean)**2 + eps)
    return ccc

def cccloss(target, pred, alpha=0.5):
    assert target.shape[-1] == 2
    ccc_p = ccc(target[:, 0], pred[:, 0])
    ccc_a = ccc(target[:, 1], pred[:, 1])
    loss = 1 - (alpha * ccc_p + (1 - alpha) * ccc_a)
    # ploss = 0.5 - alpha * ccc_p
    # aloss = 0.5 - (1 - alpha) * ccc_a
    # loss = ploss + aloss
    return loss, ccc_p, ccc_a